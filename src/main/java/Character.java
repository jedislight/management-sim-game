import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Character extends Statted implements Serializable {
    enum Stats {
        Carry,
        Toughness,
        Perception,
        Leadership,
        Athletics,
        Stealth,
        Arcana,
        History,
        Investigation,
        Survival,
        Animals,
        Healing,
        Barter,
        Evocation,
        Conjuration,
        Divination,
        Necromancy,
        Combat,
        Hunting,
        Abjuration,
        Transmutation,
        Engineering,
        Resilience
    }
    static Random random = new Random();

    String firstName = NameGenerator.generate();
    String lastName = NameGenerator.generate();
    String nickName = firstName;

    public Character() {
        List<Stats> shuffledStats = new ArrayList<Stats>(List.of(Stats.values()));
        Collections.shuffle(shuffledStats);
        for (int i = 0; i < 6; ++i) {
            stats.add(new Stat(shuffledStats.get(i).name(), startingStat()));
        }
    }

    private int startingStat() {
        return Math.abs(random.nextInt() % 5) + 1;
    }

    public int getHiringCost() {
        return getDailyWage() * 30;
    }

    public int getDailyWage() {
        return stats.stream().map(s->s.value).reduce(0, Integer::sum);
    }
}
