import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Separator;

public class CharacterWindow extends BasicWindow {
    public CharacterWindow(Character character, Button... additionalButtons) {
        super(character.nickName);

        Panel mainPanel = new Panel(new LinearLayout(Direction.VERTICAL));
        mainPanel.addComponent(new Label(character.nickName));
        mainPanel.addComponent(new Label(character.firstName + " " + character.lastName));
        for (Stat stat : character.stats) {
            mainPanel.addComponent(new Label(String.format("%s %d", stat.name, stat.value)));
        }
        mainPanel.addComponent(new Separator(Direction.VERTICAL));
        for (Button button : additionalButtons) {
            mainPanel.addComponent(button);
        }
        mainPanel.addComponent(new Button("Back", CharacterWindow.this::close));
        setComponent(mainPanel);
    }
}
