import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Company implements Serializable {
    enum AutomationLevel {
        NONE,
        SUGGEST,
        FULL
    }

    enum Roles {
        Scryer("Expands charts of known areas slowly over time using " + Character.Stats.Divination.name(), Character.Stats.Divination),
        ;

        String description;
        Character.Stats[] relatedCharacterStats;

        Roles(String description, Character.Stats... relatedCharacterStats) {
            this.description = description;
            this.relatedCharacterStats = relatedCharacterStats;
        }
    }

    String name = "4th World Expeditions";
    List<Character> roster = new ArrayList<>();
    List<Expedition> expeditions = new ArrayList<>();
    long credits = 1000;
    Date founded;
    AutomationLevel hiringAutomationLevel = AutomationLevel.NONE;
    AutomationLevel roleAssignmentAutomationLevel = AutomationLevel.NONE;
    Map<Roles, Character> roles = new HashMap<>();

    Company(Game game) {
        founded = game.date;

        for (Roles role : Roles.values()) {
            roles.put(role, null);
        }
    }

    public Company withName(String name) {
        this.name = name;
        return this;
    }

    public List<Character> selectInterestingHires(List<Character> freeAgents) {
        Map<Integer, Character> rankedCharacters = new HashMap<>();
        for(Character freeAgent : freeAgents) {
            Integer rank = 0;
            for(Stat stat : freeAgent.stats) {
                if (stat.check()) {
                    --rank;
                } else {
                    ++rank;
                }
                boolean uniqueStat = true;
                for (Character rosterCharacter : roster) {
                    Stat rosterStat = rosterCharacter.getStat(stat.name);
                    if (rosterStat != null) {
                        uniqueStat = false;
                    }
                    if (rosterStat == null || stat.challenge(rosterStat)){
                        --rank;
                    } else {
                        ++rank;
                    }
                }

                if (uniqueStat) {
                    --rank;
                }
            }
            rankedCharacters.put(rank, freeAgent);
        }

        return  rankedCharacters.keySet().stream().filter(i -> i < 0).sorted().map(rankedCharacters::get).collect(Collectors.toList());
    }

    public Character selectBestCharacterForRole(Roles role) {
        Character best = null;
        int bestStatSum = 0;
        for (Character character : roster) {
            int characterSum = 0;
            for( Character.Stats stat : role.relatedCharacterStats) {
                Stat characterStat = character.getStat(stat.name());
                if (characterStat != null) {
                    characterSum += characterStat.value;
                }
            }

            if (characterSum > bestStatSum) {
                bestStatSum = characterSum;
                best = character;
            }
        }

        return best;
    }
}
