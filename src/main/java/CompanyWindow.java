import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.dialogs.ActionListDialogBuilder;

import java.text.DateFormat;


public class CompanyWindow extends BasicWindow {
    Company company;
    public CompanyWindow(Company company, Button... additionalButtons) {
        super(company.name);
        this.company = company;

        Panel mainPanel = new Panel(new LinearLayout(Direction.VERTICAL));
        mainPanel.addComponent(new Label("Founded: " + DateFormat.getDateInstance().format(company.founded)));
        mainPanel.addComponent(new Label("C-"+company.credits));
        mainPanel.addComponent(new Button("Roster (" + company.roster.size() + ")", this::onCompanyRosterButton));
        for (Button button : additionalButtons) {
            mainPanel.addComponent(button);
        }
        mainPanel.addComponent(new Button("Back", CompanyWindow.this::close));
        setComponent(mainPanel);
    }

    private void onCompanyRosterButton() {
        ActionListDialogBuilder actionListDialogBuilder = new ActionListDialogBuilder();
        actionListDialogBuilder.setTitle("Roster - " +  company.name);
        for (Character character : company.roster) {
            actionListDialogBuilder.addAction(character.nickName, () -> CompanyWindow.this.getTextGUI().addWindowAndWait(new CharacterWindow(character)));
        }
        actionListDialogBuilder.build().showDialog(getTextGUI());
    }
}
