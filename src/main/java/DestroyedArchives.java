public class DestroyedArchives extends Event{

    public DestroyedArchives() {
        requirements.put(Location.Stats.Archives.name(), 2);
    }

    @Override
    void run(Expedition expedition) {
        newsTag = "Traces of archives found in " + expedition.location.name;
    }
}
