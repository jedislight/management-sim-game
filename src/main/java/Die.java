import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Die {
    static Random random = new Random();

    static public int roll2d6() {
        return rollD6() + rollD6();
    }

    static public int rollD6() {
        return Math.abs(random.nextInt() % 6) + 1;
    }

    static public <T extends Object> T sample(Collection<T> collection) {
        assert collection != null;
        assert !collection.isEmpty();
        List<T> shuffledList = new ArrayList<T>(collection);
        Collections.shuffle(shuffledList);

        return shuffledList.get(0);
    }
}
