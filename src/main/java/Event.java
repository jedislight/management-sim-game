import java.util.HashMap;
import java.util.Map;

public abstract class Event {
    Map<String, Integer> requirements = new HashMap<>();
    Event followUp = null;
    String newsTag = null;
    String newsBody = null;

    abstract void run(Expedition expedition);
}
