import java.util.ArrayList;
import java.util.List;

public class Expedition {
    Company company;
    List<Character> characters;
    Location location;
    int supplies = 0;

    Expedition(Company company, List<Character> characters, Location location) {
        this.company = company;
        this.characters = new ArrayList<>(characters);
        this.location = location;
        for (Character character : characters) {
            supplies += character.getStatValue(Character.Stats.Carry.name()) + 1;
        }
    }
}
