import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Game implements java.io.Serializable {
    Date lastModified = new Date();
    Date date;
    List<Character> freeAgents = new ArrayList<>();
    Company company;
    List<Company> rivalCompanies = new ArrayList<>();
    HashMap<Date, List<NewsEvent>> news = new HashMap<>();
    UiOptions uiOptions = new UiOptions();
    Location rootLocation = new Location();
    List<Location> locations = new ArrayList<>();
    Random random = new Random();
    List<Event> events = new ArrayList<>();

    transient Path filepath = null;

    public Game() {
        date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, 1300);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        date = c.getTime();

        company = new Company(this);
        rivalCompanies.add(new Company(this).withName("Sand Seekers"));
        rivalCompanies.add(new Company(this).withName("Delvers"));
        rivalCompanies.add(new Company(this).withName("Black Iron Mercenaries"));

        for (Company company : rivalCompanies) {
            company.hiringAutomationLevel = company.roleAssignmentAutomationLevel = Company.AutomationLevel.FULL;
        }


        final int STARTING_ROSTER_SIZE = 0;
        //noinspection ConstantConditions
        for (int i = 0; i < STARTING_ROSTER_SIZE; ++i) {
            company.roster.add(new Character());
        }

        final int STARTING_FREE_AGENTS_POOL_SIZE = 10;
        for (int i = 0; i < STARTING_FREE_AGENTS_POOL_SIZE; ++i) {
            freeAgents.add(new Character());
        }

        rootLocation.setStat(Location.Stats.Archives.name(), 3);
        {
            List<Location.Stats> shuffledStats = new ArrayList<>(List.of(Location.Stats.values()));
            Collections.shuffle(shuffledStats, random);
            rootLocation.setStat(shuffledStats.get(0).name(), 5);
        }
        rootLocation.name = "Archivus";
        for (Company company : allCompanies()) {
            rootLocation.charts.put(company, rootLocation.stats.stream().map((s) -> s.name).collect(Collectors.toList()));
        }
        //temp
        Location tempLocation = new Location(rootLocation);
        tempLocation.name = "The Titan Volcano";
        tempLocation.stats.add(new Stat(Location.Stats.Magma.name(), 7));
        for (Company company : allCompanies()) {
            tempLocation.charts.put(company, new ArrayList<>(List.of(Location.Stats.Magma.name())));
        }
        locations.add(tempLocation);
        //temp
        locations.add(rootLocation);

        events.add(new DestroyedArchives());
    }

    void save(Path filepath) throws IOException {
        lastModified = new Date();
        this.filepath = filepath;
        FileOutputStream fileOut = new FileOutputStream(filepath.toString());
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this);
        out.close();
        fileOut.close();
    }

    static Game load(Path filepath) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(filepath.toString());
        ObjectInputStream in = new ObjectInputStream(fileIn);
        Game game = (Game) in.readObject();
        in.close();
        fileIn.close();
        game.filepath = filepath;

        return game;
    }

    public void advanceDay() {
        for (Company company : allCompanies()) {
            companyPerformsDailyTurn(company);
        }

        if (freeAgents.size() < Die.roll2d6()) {
            Character character = new Character();
            freeAgents.add(character);
            addNews(String.format("New free agent available %s, focus: %s", character.nickName, character.stats.stream().max(Comparator.comparingInt(a -> a.value)).get().name));
        }

        // Keep at end
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_YEAR, 1);
        date = c.getTime();
    }

    private void companyPerformsDailyTurn(Company company) {
        for (Character character : company.roster) {
            company.credits -= character.getDailyWage();
        }

        Character companyScryer = company.roles.get(Company.Roles.Scryer);
        if (companyScryer != null) {
            Stat companyScryerStat = companyScryer.getStat(Character.Stats.Divination.name());
            if (companyScryerStat != null && companyScryerStat.check()) {
                List<Location> knownLocations = locations.stream().filter(l -> l.charts.containsKey(company)).collect(Collectors.toList());
                Location selectedLocation = Die.sample(knownLocations);
                Stat scryTargetStat = Die.sample(selectedLocation.stats);
                boolean alreadyKnown = selectedLocation.charts.get(company).contains(scryTargetStat.name);
                if (!alreadyKnown) {
                    selectedLocation.charts.get(company).add(scryTargetStat.name);
                    statTrainingOpprotunity(company, companyScryer, companyScryerStat.name);
                    if (company == this.company) {
                        addNews(String.format("%s charted %s - %d in %s via Divination", companyScryer.nickName, scryTargetStat.name, scryTargetStat.value, selectedLocation.name));
                    }
                }
            }
        }

        if (company.hiringAutomationLevel != Company.AutomationLevel.NONE) {
            List<Character> interestingHires = company.selectInterestingHires(freeAgents);
            for (Character interestingHire : interestingHires) {
                if (interestingHire.getHiringCost() < company.credits) {
                    companyHiresFreeAgent(company, interestingHire);
                }
            }
        }

        if (company.roleAssignmentAutomationLevel != Company.AutomationLevel.NONE) {
            for (Company.Roles role : Company.Roles.values()) {
                Character best = company.selectBestCharacterForRole(role);
                if (best != company.roles.get(role)) {
                    companyPromotesCharacterIntoRole(company, best, role);
                }
            }
        }

        //TEMP
        if (company.expeditions.isEmpty() && !company.roster.isEmpty()) {
            List<Location> knownLocations = locations.stream().filter(l->l.charts.containsKey(company)).collect(Collectors.toList());
            Location embarkLocation = Die.sample(knownLocations);
            company.expeditions.add(new Expedition(company, company.roster, embarkLocation));
            if (company == this.company) {
                addNews(String.format("Expedition auto added to location: %s", embarkLocation.name));
            }
        }
        //TEMP
        for (Expedition expedition : new ArrayList<>(company.expeditions)) {
            expedition.supplies -= expedition.characters.size();
            for (Character character : expedition.characters) {
                if (character.checkTrained(Character.Stats.Conjuration.name())) {
                    expedition.supplies += 1;
                }
            }

            final int newAreaDifficulty; {
                int sum = 0;
                sum = (int) locations.stream().filter(l -> l.parent == expedition.location).count();
                sum += expedition.location.getLocationChain().size();
                newAreaDifficulty = sum;
            }
            if (newAreaDifficulty < 11) {
                boolean foundSubRegion = expedition.characters.stream().anyMatch((c) -> c.challange(Character.Stats.Investigation.name(), new Stat("Temp", newAreaDifficulty)));
                if (foundSubRegion) {
                    Location newLocation = new Location(expedition.location);
                    newLocation.name = LocationNameGenerator.generate(newLocation);
                    for(int i = 0; i < Die.rollD6(); ++i) {
                        newLocation.setStat(Die.sample(List.of(Location.Stats.values())).name(), Die.roll2d6());
                    }
                    newLocation.charts.put(company, new ArrayList<>());
                    locations.add(newLocation);

                    if (company == this.company) {
                        addNews(String.format("Expedition in %s discovered new area %s", expedition.location.name, newLocation.name));
                    }
                }
            }

            if (expedition.supplies <= 0) {
                company.expeditions.remove(expedition);
                if (company == this.company) {
                    addNews(String.format("An expedition ran out of supplies and has returned."));
                }
            }
            Event event = null;
            for (Event e : events) {
                boolean allPassed = true;
                for (String s : e.requirements.keySet()) {
                    if (!expedition.location.challangeTrained(s,new Stat(s, e.requirements.get(s)))) {
                        allPassed = false;
                        break;
                    }

                    if (allPassed) {
                        event = e;
                    }
                }
            }

            if (event != null) {
                event.run(expedition);
                if (expedition.company == this.company) {
                    addNews(event.newsTag);
                }
            }
        }

    }

    private void statTrainingOpprotunity(Company company, Character character, String statName) {
        Stat stat = character.getStat(statName);
        if (stat == null) {
            if (Die.roll2d6() == 2){
                character.stats.add(new Stat(statName, 1));
                if (company == this.company) {
                    addNews(String.format("%s is now familiar with %s", character.nickName, statName));
                }
            }
        } else if (!stat.check()) {
            stat.improvement += 1;
            if (stat.improvement >= stat.value) {
                stat.value += 1;
                stat.improvement = 0;
                if (company == this.company) {
                    addNews(String.format("%s has improved in %s", character.nickName, statName));
                }
            } else if (company == this.company) {
                addNews(String.format("%s is showing improvement in %s", character.nickName, statName));
            }
        }
    }

    public List<Company> allCompanies() {
        List<Company> allCompanies = new ArrayList<>();
        allCompanies.add(company);
        allCompanies.addAll(rivalCompanies);
        return allCompanies;
    }

    public void companyHiresFreeAgent(Company company, Character character) {
        freeAgents.remove(character);
        company.roster.add(character);
        company.credits -= character.getHiringCost();
        addNews(String.format("%s hires %s for C-%d", company.name, character.nickName, character.getHiringCost()));
    }

    public List<NewsEvent> getTodaysNews() {
        List<NewsEvent> events = news.get(date);
        if (events == null) {
            return Collections.EMPTY_LIST;
        }

        return events;
    }

    public List<NewsEvent> getYesterdaysNews() {
        List<NewsEvent> events = news.get(getYesterdaysDate());
        if (events == null) {
            return Collections.EMPTY_LIST;
        }

        return events;
    }

    public Date getYesterdaysDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_YEAR, -1);
        return c.getTime();
    }

    public void addNews(String description) {
        if (!news.containsKey(date)) {
            news.put(date, new ArrayList<>());
        }

        news.get(date).add(new NewsEvent(this, description));
    }

    public void companyPromotesCharacterIntoRole(Company company, Character character, Company.Roles role) {
        Character existingCharacter = company.roles.get(role);
        if (existingCharacter != null && character != null) {
            addNews(String.format("%s promoted to %s for %s, replacing %s", character.nickName, role.name(), company.name, existingCharacter.nickName));
        } else if (existingCharacter == null && character != null) {
            addNews(String.format("%s promoted to %s for %s", character.nickName, role.name(), company.name));
        } else {
            addNews(String.format("%s no longer %s for %s", character.nickName, role.name(), company.name));
        }
        company.roles.put(role, character);
    }
}
