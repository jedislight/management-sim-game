import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Separator;
import com.googlecode.lanterna.gui2.dialogs.ActionListDialogBuilder;
import com.googlecode.lanterna.gui2.dialogs.FileDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class GameWindow extends BasicWindow {
    Game game;
    Panel statusPanel;
    Panel newsPanel;
    Label newsLabel;
    Thread timerThread60fps;
    boolean closing = false;
    String dailyNewsTicker = "";
    int tickerOffset = 0;

    public GameWindow(Game game) {
        super("RPG-SIM");
        this.game = game;
        setHints(List.of(Hint.FULL_SCREEN));

        Panel mainPanel = new Panel(new LinearLayout(Direction.VERTICAL));
        statusPanel = new Panel(new LinearLayout(Direction.HORIZONTAL));
        newsPanel = new Panel(new LinearLayout(Direction.HORIZONTAL));
        newsLabel = new Label("");
        newsPanel.addComponent(newsLabel);
        mainPanel.addComponent(statusPanel);
        mainPanel.addComponent(new Button("Next Day", this::onEndDayButton));
        mainPanel.addComponent(new Button("News", this::onNewsButton));
        mainPanel.addComponent(new Button("Location Charts", this::onChartsButton));
        mainPanel.addComponent(new Button("Roles", this::onRolesButton));
        mainPanel.addComponent(new Button("Roster", this::onRosterButton));
        mainPanel.addComponent(new Button("Free Agents", this::onFreeAgentsButton));
        mainPanel.addComponent(new Button("Other Companies", this::onViewOtherCompaniesButton));
        mainPanel.addComponent(new Button("Save", this::onSaveButton));
        mainPanel.addComponent(new Button("Exit", this::onExitButton));
        mainPanel.addComponent(new Separator(Direction.VERTICAL));
        mainPanel.addComponent(newsPanel.withBorder(com.googlecode.lanterna.gui2.Borders.singleLineBevel("News")));


        setupStatusPanel(game);

        refresh();
        setComponent(mainPanel);

        timerThread60fps = new Thread(this::newsTickerUpdateThread);
        timerThread60fps.start();
    }

    private void onRolesButton() {
        ActionListDialogBuilder roleListActionDialogBuilder = new ActionListDialogBuilder();
        roleListActionDialogBuilder.setTitle("Roles");
        for (Company.Roles role : game.company.roles.keySet()) {
            Character roleAssignee = game.company.roles.get(role);
            String roleAssigneeString = "Vacant";
            int roleStatValue = 0;
            if (roleAssignee != null) {
                roleAssigneeString = roleAssignee.nickName;
                for (Character.Stats stat : role.relatedCharacterStats) {
                    Stat characterStat = roleAssignee.getStat(stat.name());
                    if (characterStat != null) {
                        roleStatValue += characterStat.value;
                    }
                }
            }
            roleListActionDialogBuilder.addAction(
                    String.format("%s - %d - %s", role.name(), roleStatValue, roleAssigneeString),
                    () -> {
                        ActionListDialogBuilder roleAssignmentDialogBuilder = new ActionListDialogBuilder();
                        roleAssignmentDialogBuilder.setTitle(role.name() + " Assignment");
                        for (Character character : game.company.roster) {
                            roleAssignmentDialogBuilder.addAction(character.nickName, () -> game.companyPromotesCharacterIntoRole(game.company, character, role));
                        }
                        roleAssignmentDialogBuilder.build().showDialog(getTextGUI());
                    }
            );
        }

        roleListActionDialogBuilder.build().showDialog(getTextGUI());
        refresh();
    }

    private void onChartsButton() {
        getTextGUI().addWindowAndWait(new LocationWindow(this.game.rootLocation, this.game.company, this.game));
    }

    private void newsTickerUpdateThread() {
        while (!closing) {
            ++tickerOffset;
            if (tickerOffset >= dailyNewsTicker.length()) {
                tickerOffset = 0;
            }
            newsLabel.setText(dailyNewsTicker.substring(tickerOffset));
            try {
                //noinspection BusyWait
                Thread.sleep((long) 1000 / 60 * game.uiOptions.tickerDelay);
            } catch (InterruptedException e) {
                // pass
            }
        }
    }

    @Override
    public void close() {
        super.close();
        closing = true;
        try {
            timerThread60fps.interrupt();
            timerThread60fps.join(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
            timerThread60fps.stop();
        }
    }

    private void onNewsButton() {
        ActionListDialogBuilder actionListDialogBuilder = new ActionListDialogBuilder();
        actionListDialogBuilder.setTitle("News - " + DateFormat.getDateInstance().format(game.getYesterdaysDate()));
        for (NewsEvent event : game.getYesterdaysNews()) {
            actionListDialogBuilder.addAction(
                event.tagline,
                () -> {
                    MessageDialog.showMessageDialog(getTextGUI(), DateFormat.getDateInstance().format(event.date) + " " + event.tagline, event.body);
                    getTextGUI().getActiveWindow().close();
                }
            );
        }
        actionListDialogBuilder.build().showDialog(getTextGUI());
        refresh();
    }

    private void onViewOtherCompaniesButton() {
        ActionListDialogBuilder actionListDialogBuilder = new ActionListDialogBuilder();
        actionListDialogBuilder.setTitle("Companies");
        for (Company company : game.allCompanies()) {
            actionListDialogBuilder.addAction(
                    company.name,
                    () -> GameWindow.this.getTextGUI().addWindowAndWait(new CompanyWindow(company))
            );
        }
        actionListDialogBuilder.build().showDialog(getTextGUI());
        refresh();
    }

    private void setupStatusPanel(Game game) {
        statusPanel.addComponent(new RefreshableLabel() {
            @Override
            public void refresh() {
                this.setText(game.company.name);
            }
        });

        statusPanel.addComponent(new RefreshableLabel() {
            @Override
            public void refresh() {
                this.setText(DateFormat.getDateInstance().format(game.date));
            }
        });

        statusPanel.addComponent(new RefreshableLabel() {
            @Override
            public void refresh() {
                this.setText(String.format("C-%d", game.company.credits));
            }
        });
    }

    private void onFreeAgentsButton() {
        ActionListDialogBuilder actionListDialogBuilder = new ActionListDialogBuilder();
        actionListDialogBuilder.setTitle("Roster");
        for (Character character : game.freeAgents) {
            actionListDialogBuilder.addAction(
                    String.format("%s (%s) - C-%d",character.nickName, character.getHighestStat().name, character.getHiringCost()),
                    () -> {
                        Button hireButton = new Button("Hire - C-" + character.getHiringCost(), () -> {
                            GameWindow.this.game.companyHiresFreeAgent(GameWindow.this.game.company, character);
                            GameWindow.this.getTextGUI().getActiveWindow().close();
                        });
                        CharacterWindow characterWindow = new CharacterWindow(character, hireButton);
                        GameWindow.this.getTextGUI().addWindowAndWait(characterWindow);
                    });
        }
        actionListDialogBuilder.build().showDialog(getTextGUI());
        refresh();
    }

    private void onRosterButton() {
        ActionListDialogBuilder actionListDialogBuilder = new ActionListDialogBuilder();
        actionListDialogBuilder.setTitle("Roster");
        for (Character character : game.company.roster) {
            actionListDialogBuilder.addAction(character.nickName, () -> GameWindow.this.getTextGUI().addWindowAndWait(new CharacterWindow(character)));
        }
        actionListDialogBuilder.build().showDialog(getTextGUI());
        refresh();
    }

    private void onEndDayButton() {
        game.advanceDay();
        refresh();
    }

    private void refreshNewsTickerContent() {
        dailyNewsTicker = game.getYesterdaysNews().stream().map(ne->ne.tagline).collect(Collectors.joining(" | "));
    }

    private void refresh() {
        for(Component component : statusPanel.getChildren()) {
            if (component instanceof IRefreshable) {
                ((IRefreshable)component).refresh();
            }
        }
        refreshNewsTickerContent();
    }

    private void onSaveButton() {
        Path gamePath = game.filepath;
        File defaultFile = null;
        if (gamePath != null) {
            defaultFile = gamePath.toFile();
        }
        FileDialog fileDialog = new FileDialog("Save Game", "Select a filepath to save the game", "Action Label?", TerminalSize.ONE, false, defaultFile);
        File dialogResult = fileDialog.showDialog(GameWindow.this.getTextGUI());
        if (dialogResult != null) {
            try {
                GameWindow.this.game.save(dialogResult.toPath());
            } catch (IOException e) {
                com.googlecode.lanterna.gui2.dialogs.MessageDialog.showMessageDialog(GameWindow.this.getTextGUI(), "Error saving game", e.getClass().getName() +":"+ e.getMessage(), MessageDialogButton.OK);
            }
        }
        refresh();
    }

    private void onExitButton() {
        GameWindow.this.close();
    }
}
