
public interface IRefreshable {
    void refresh();
}
