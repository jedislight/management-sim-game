import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Location extends Statted implements Serializable {
    enum Stats {
        Forage,
        Dinosaurs,
        Water,
        Guardians,
        Traps,
        Seals,
        Gardens,
        Ruins,
        Forests,
        Plains,
        Urban,
        Undead,
        Magma,
        Mountains,
        Swamp,
        Archives

    }
    Location parent;
    String name;
    Map<Company, List<String>> charts = new HashMap<>();

    Location() {}
    Location(Location parent) {
        this.parent = parent;
        for (Stat stat : parent.stats){
            setStat(stat.name, stat.value);
        }
    }

    int getDepth() {
        return getLocationChain().size();
    }

    List<Location> getLocationChain() {
        List<Location> chain = new ArrayList<>();
        int depth = 0;
        Location iterator = this;
        do {
            chain.add(iterator);
            iterator = iterator.parent;
        } while (iterator != null);

        Collections.reverse(chain);

        return chain;
    }

}
