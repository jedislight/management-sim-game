import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LocationNameGenerator {
    static Map<String, List<String>> locationStatNouns;
    static Map<String, List<String>> locationStatAdjectives;

    static {
        locationStatNouns = new HashMap<>();
        locationStatAdjectives = new HashMap<>();

        locationStatNouns.put(Location.Stats.Forage.name(), List.of("Fields", "Bounty", "Harvest"));
        locationStatNouns.put(Location.Stats.Dinosaurs.name(), List.of("Terra", "Grounds"));
        locationStatNouns.put(Location.Stats.Water.name(), List.of("Sea", "River", "Lake", "Falls"));
        locationStatNouns.put(Location.Stats.Guardians.name(), List.of("Fortress", "Factory", "Manufactury"));
        locationStatNouns.put(Location.Stats.Traps.name(), List.of("Gauntlet", "Dungeon"));
        locationStatNouns.put(Location.Stats.Seals.name(), List.of("Runewalk", "Zone", "Barrier"));
        locationStatNouns.put(Location.Stats.Gardens.name(), List.of("Garden", "Atrium"));
        locationStatNouns.put(Location.Stats.Ruins.name(), List.of("Ruins"));
        locationStatNouns.put(Location.Stats.Forests.name(), List.of("Forest", "Jungle", "Grove"));
        locationStatNouns.put(Location.Stats.Plains.name(), List.of("Fields", "Grassland"));
        locationStatNouns.put(Location.Stats.Urban.name(), List.of("City", "Camp", "Metropolis"));
        locationStatNouns.put(Location.Stats.Undead.name(), List.of("Crypt", "Graveyard", "Mausoleum", "Sepulcher"));
        locationStatNouns.put(Location.Stats.Magma.name(), List.of("Volcano", "Flows", "Springs"));
        locationStatNouns.put(Location.Stats.Swamp.name(), List.of("Swamp", "Marsh", "Bog"));
        locationStatNouns.put(Location.Stats.Archives.name(), List.of("Libraries", "Museum", "Repository", "Archives"));
        locationStatNouns.put(Location.Stats.Mountains.name(), List.of("Mountain", "Peak", "Hill", "Crags"));

        locationStatAdjectives.put(Location.Stats.Forage.name(), List.of("Bountiful", "Lush", "Vibrant"));
        locationStatAdjectives.put(Location.Stats.Dinosaurs.name(), List.of("Prehistoric", "Jurassic", "Ancient", "Fossilized"));
        locationStatAdjectives.put(Location.Stats.Water.name(), List.of("Blue", "Soaked", "Wet", "Misty", "Foggy"));
        locationStatAdjectives.put(Location.Stats.Guardians.name(), List.of("Mechanical", "Golemoid", "Clanking"));
        locationStatAdjectives.put(Location.Stats.Traps.name(), List.of("Dangerous", "Trapped"));
        locationStatAdjectives.put(Location.Stats.Seals.name(), List.of("Arcane", "Mystic", "Magical"));
        locationStatAdjectives.put(Location.Stats.Gardens.name(), List.of("Green", "Curated", "Beautiful"));
        locationStatAdjectives.put(Location.Stats.Ruins.name(), List.of("Destroyed", "Decayed"));
        locationStatAdjectives.put(Location.Stats.Forests.name(), List.of("Wooded", "Leafy"));
        locationStatAdjectives.put(Location.Stats.Plains.name(), List.of("Grassy", "Flat"));
        locationStatAdjectives.put(Location.Stats.Urban.name(), List.of("Abandoned", "Urban"));
        locationStatAdjectives.put(Location.Stats.Undead.name(), List.of("Deathly", "Noxious", "Soulless", "Black"));
        locationStatAdjectives.put(Location.Stats.Magma.name(), List.of("Hot", "Red", "Molten"));
        locationStatAdjectives.put(Location.Stats.Swamp.name(), List.of("Submerged", "Moist", "Bubbling"));
        locationStatAdjectives.put(Location.Stats.Archives.name(), List.of("Pristine", "Clean", "Arranged"));
        locationStatAdjectives.put(Location.Stats.Mountains.name(), List.of("Rocky", "High", "Titan"));

    }

    static public String generate(Location location) {
        String highStatName = location.getHighestStat().name;
        List<String> nounList = locationStatNouns.get(highStatName).stream().collect(Collectors.toList());
        System.out.println(String.format("HighStat: %s", highStatName));
        Stat stat = Die.sample(location.stats);
        System.out.println(String.format("RandomStat: %s", stat.name));
        List<String> adjectiveList = locationStatAdjectives.get(stat.name).stream().collect(Collectors.toList());

        String prefix = "";
        if (Die.rollD6() <= 2) {
            prefix = "The ";
        }
        assert adjectiveList != null;
        assert nounList != null;
        return prefix + (Die.sample(adjectiveList) + " " + Die.sample(nounList));
    }
}
