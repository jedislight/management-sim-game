import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Separator;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;

import java.util.List;
import java.util.stream.Collectors;

public class LocationWindow extends BasicWindow {
    Location location;
    public LocationWindow(Location location, Company company, Game game, Button... additionalButtons) {
        super(location.getLocationChain().stream().map(l->l.name).collect(Collectors.joining(" - ")));
        this.location = location;

        Panel mainPanel = new Panel(new LinearLayout(Direction.VERTICAL));

        List<String> charted = location.charts.get(company);
        if (charted != null) {
            for (Stat stat : location.stats) {
                if (charted.contains(stat.name)) {
                    mainPanel.addComponent(new Label(String.format("%s %d", stat.name, stat.value)));
                }
            }
            mainPanel.addComponent(new Separator(Direction.VERTICAL));
        }
        for (Button button : additionalButtons) {
            mainPanel.addComponent(button);
        }
        mainPanel.addComponent(new Separator(Direction.VERTICAL));
        for (Location child : game.locations) {
            if (child.parent == location && child.charts.containsKey(company)){
                mainPanel.addComponent(new Button("Subregion: " + child.name, () -> LocationWindow.this.getTextGUI().addWindowAndWait(new LocationWindow(child, company, game))));
            }
        }
        mainPanel.addComponent(new Separator(Direction.VERTICAL));
        mainPanel.addComponent(new Button("Back", LocationWindow.this::close));
        mainPanel.addComponent(new Button("Close", () -> {
            WindowBasedTextGUI textGUI = getTextGUI();
            while(textGUI.getActiveWindow() instanceof LocationWindow){
                textGUI.getActiveWindow().close();
            }
        }));
        setComponent(mainPanel);
    }
}
