import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.TerminalEmulatorAutoCloseTrigger;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Terminal term = new DefaultTerminalFactory()
                .addTerminalEmulatorFrameAutoCloseTrigger(TerminalEmulatorAutoCloseTrigger.CloseOnExitPrivateMode)
                .createTerminal();
        Screen screen = new TerminalScreen(term);
        WindowBasedTextGUI gui = new MultiWindowTextGUI(screen);
        screen.startScreen();
        // use GUI here until the GUI wants to exit
        MainWindow mainWindow = new MainWindow();
        gui.addWindow(mainWindow);
        mainWindow.waitUntilClosed();
        screen.stopScreen();
    }
}
