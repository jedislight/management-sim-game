import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.dialogs.FileDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainWindow extends BasicWindow {
    public MainWindow() {
        super("RPG-SIM");
        setHints(List.of(Hint.FULL_SCREEN));
        Panel mainPanel = new Panel(new LinearLayout(Direction.VERTICAL));
        mainPanel.addComponent(new Button("Continue", this::onNotImplementedButton));
        mainPanel.addComponent(new Button("New Game", this::onNewGameButton));
        mainPanel.addComponent(new Button("Load", this::onLoadGameButton));
        mainPanel.addComponent(new Button("Exit", this::onExitButton));

        setComponent(mainPanel);
    }

    private void onNotImplementedButton() {
        MessageDialog.showMessageDialog(MainWindow.this.getTextGUI(), "Not Implemented", "Yet...", MessageDialogButton.OK);
    }

    private void onExitButton(){
        MainWindow.this.close();
    }

    private void onNewGameButton() {
        Game game = new Game();
        GameWindow gameWindow = new GameWindow(game);
        MainWindow.this.getTextGUI().addWindowAndWait(gameWindow);
    }

    private void onLoadGameButton() {
        FileDialog fileDialog = new FileDialog("Load Game", "Select a filepath to load the game", "Action Label?", TerminalSize.ONE, false, null);
        File dialogResult = fileDialog.showDialog(MainWindow.this.getTextGUI());
        Game game = null;
        if (dialogResult != null) {
            try {
                game = Game.load(dialogResult.toPath());
            } catch (IOException | ClassNotFoundException e) {
                com.googlecode.lanterna.gui2.dialogs.MessageDialog.showMessageDialog(MainWindow.this.getTextGUI(), "Error Loading game", e.getClass().getName()+":"+ e.getLocalizedMessage(), MessageDialogButton.OK);
            }
        }
        if (game != null) {
            GameWindow gameWindow = new GameWindow(game);
            MainWindow.this.getTextGUI().addWindowAndWait(gameWindow);
        }
    }
}