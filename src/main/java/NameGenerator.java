import java.util.Random;

@SuppressWarnings("SpellCheckingInspection")
public class NameGenerator {
    static Random random = new Random();
    static String[][] syllables = new String[][]{
            {"A","bb","a"},
            {"Er","gg","ath"},
            {"Me","ln","id"},
            {"She","rp","org"},
            {"Ada","bl","ab"},
            {"Ere","gl","ave"},
            {"Mi","lord","if"},
            {"Sho","rr","ort"},
            {"Aki","bold","ac"},
            {"Eri","gord","ea"},
            {"Mo","ls","ik"},
            {"So","rush","os"},
            {"Al","br","ace"},
            {"Ero","gr","eb"},
            {"Na","matt","il"},
            {"Sta","salk","osh"},
            {"Ali","bran","ach"},
            {"Fa","gs","ec"},
            {"Ne","mend","im"},
            {"Ste","sass","ot"},
            {"Alo","can","ad"},
            {"Fe","h","ech"},
            {"No","mm","in"},
            {"Sti","sc","oth"},
            {"Ana","carr","adle"},
            {"Fi","hall","ed"},
            {"O","ms","ion"},
            {"Stu","sh","ottle"},
            {"Ani","ch","af"},
            {"Fo","hark","ef"},
            {"Ol","nd","ir"},
            {"Ta","sp","ove"},
            {"Ba","cinn","ag"},
            {"Fra","hill","eh"},
            {"Or","nett","is"},
            {"Tha","ss","ow"},
            {"Be","ck","ah"},
            {"Gla","hork","ek"},
            {"Pa","ng","ish"},
            {"The","st","ox"},
            {"Bo","ckl","ai"},
            {"Gro","jenn","el"},
            {"Pe","nk","it"},
            {"Tho","tall","ud"},
            {"Bra","ckr","ak"},
            {"Ha","kell","elle"},
            {"Pi","nn","ith"},
            {"Ti","tend","ule"},
            {"Bro","cks","aker"},
            {"He","kill","elton"},
            {"Po","nodd","ive"},
            {"To","told","umber"},
            {"Cha","dd","al"},
            {"Hi","kk","em"},
            {"Pra","ns","ob"},
            {"Tra","v","un"},
            {"Chi","dell","ale"},
            {"Illia","kl","en"},
            {"Qua","nt","och"},
            {"Tri","vall","under"},
            {"Da","dr","am"},
            {"Ira","klip","end"},
            {"Qui","part","od"},
            {"Tru","w","undle"},
            {"De","ds","an"},
            {"Ja","kr","ent"},
            {"Quo","pelt","odin"},
            {"Ul","wall","unt"},
            {"Do","fadd","and"},
            {"Jo","krack","enton"},
            {"Ra","pl","oe"},
            {"Ur","wild","ur"},
            {"Dra","fall","ane"},
            {"Ka","ladd","ep"},
            {"Re","pp","of"},
            {"Va","will","us"},
            {"Dro","farr","ar"},
            {"Ki","land","er"},
            {"Ro","ppr","oh"},
            {"Vo","x","ust"},
            {"Eki","ff","ard"},
            {"Kra","lark","esh"},
            {"Sa","pps","ol"},
            {"Wra","y","ut"},
            {"Eko","fill","ark"},
            {"La","ld","ess"},
            {"Sca","rand","olen"},
            {"Xa","yang"},
            {"Ele","fl","art"},
            {"Le","ldr","ett"},
            {"Sco","rd","omir"},
            {"Xi","yell"},
            {"Eli","fr","ash"},
            {"Lo","lind","ic"},
            {"Se","resh","or"},
            {"Zha","z"},
            {"Elo","genn","at"},
            {"Ma","ll","ich"},
            {"Sha","rn","orb"},
            {"Zho","zenn"}
    };

    public static String generate() {
        String name = "";
        int sIndex = 0;
        while(true) {
            int index = Math.abs(random.nextInt()) % syllables.length;
            if (syllables[index].length > sIndex) {
                name += syllables[index][sIndex++];
            } else {
                break;
            }
        }

        return name;
    }
}
