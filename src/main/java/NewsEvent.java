import java.io.Serializable;
import java.util.Date;

public class NewsEvent implements Serializable {
    Date date;
    String tagline;
    String body;

    public NewsEvent(Game game, String line){
        date = game.date;
        tagline = body = line;
    }
}
