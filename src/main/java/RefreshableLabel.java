import com.googlecode.lanterna.gui2.Label;

public abstract class RefreshableLabel extends Label implements IRefreshable {
    RefreshableLabel() {
        super("Refreshable Default Text");
        refresh();
    }
}
