import java.io.Serializable;

public class Stat implements Serializable {
    String name;
    int value;
    int improvement = 0;

    public Stat(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public boolean check(){
        int roll = Die.roll2d6();
        // 12 == auto fail
        // 2 == auto pass
        // else roll <= value pass
        return roll!= 12 && (roll <= value || roll == 2);
    }

    public boolean challenge(Stat other) {
        while(true) {
            boolean us = check();
            boolean them = other.check();
            if (us != them) {
                return us;
            }
        }
    }
}
