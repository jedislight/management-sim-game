import java.util.ArrayList;
import java.util.List;

public class Statted {
    List<Stat> stats = new ArrayList<>();

    public Stat getStat(String name) {
        for (Stat stat : stats) {
            if (stat.name.equals(name)){
                return stat;
            }
        }
        return null;
    }

    public void setStat(String name, int value) {
        Stat stat = getStat(name);
        if (stat != null) {
            stat.value = value;
        } else {
            stats.add(new Stat(name, value));
        }
    }

    public Stat getHighestStat() {
        Stat highest = stats.get(0);
        for (Stat stat : stats) {
            if (stat.value > highest.value) {
                highest = stat;
            }
        }

        return highest;
    }

    public int getStatValue(String statName) {
        Stat stat = getStat(statName);
        if(stat == null) {
            return 0;
        }

        return stat.value;
    }

    public boolean checkTrained(String name) {
        Stat stat = getStat(name);
        if(stat == null) {
            return false;
        }

        return stat.check();
    }

    public boolean check(String name) {
        Stat stat = getStat(name);
        if(stat == null) {
            if(Die.roll2d6() == 2) {
                return true;
            }
        }

        return stat.check();
    }

    public boolean challange(String name, Stat other) {
        Stat stat = getStat(name);
        if(stat == null) {
            stat = new Stat(name, 0);
        }

        return stat.challenge(other);
    }

    public boolean challangeTrained(String name, Stat other) {
        Stat stat = getStat(name);
        if(stat == null) {
            return false;
        }

        return stat.challenge(other);
    }
}
